RJR Chair designs
v1.1 by Mario Alessiani
release date 15/06/21

Project website at: http://www.slowd.it/en/progetti-speciali/100-different-copies/

RJR is distributed under the CERN 1.2 license, that means you are free to manufacture
the product and modify or re-distrbute the design files under the same license.

According to the license, you are invited to inform us and any other Licensor who has indicated his wish 
to receive this information about the type, quantity and dates of production 
of Products the Licensee has (had) manufactured.

